#include "menu.h"

using namespace std;

bool fullscreen = false;
bool jatek_vege = false;

HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
template<typename T>
void color(T szoveg_szin, T hatter_szin) {
	SetConsoleTextAttribute(handle, szoveg_szin + hatter_szin * 16);
}

void helyez_kurzor(int x, int y) {   //helyezi a kurzort a console egysegeiben(pixeleiben)
	COORD kordinatak;
	kordinatak.X = x;
	kordinatak.Y = y;
	SetConsoleCursorPosition(handle, kordinatak);
}

void size_screenBuffer(int& oszlopok_szama_X, int& sorok_szama_Y) {   // lelehet kerni a kepernyo bufferet, figyelem ez nem az aktualis kepernyo merete
	CONSOLE_SCREEN_BUFFER_INFO screenBufinfo;                        // hanem amekkorat letrehozott a console ablak, peldaul ha a sorok szama be van allitva
	GetConsoleScreenBufferInfo(handle, &screenBufinfo);              //9000 -re hogy annyi sort tudjon kiirni akkor ez a fuggveny azt adja vissza
	oszlopok_szama_X = screenBufinfo.dwSize.X;
	sorok_szama_Y = screenBufinfo.dwSize.Y;
}

void cursor_pos_screenBuffer(int& x, int& y) {                    // visszaadja, hogy a kursor hol van jelenleg a kepernyon
	CONSOLE_SCREEN_BUFFER_INFO screenBufinfo;
	GetConsoleScreenBufferInfo(handle, &screenBufinfo);
	x = screenBufinfo.dwCursorPosition.X;
	y = screenBufinfo.dwCursorPosition.Y;
}

void window_size(int& szelesseg, int& hosszusag, int y_betu_meret, int x_betu_meret) {  //ez lekeri az aktualis ablak meretet pixelekben es ezt at kell alakitani a console egysegeiben
	HBITMAP hBitmap = NULL;                                                             // amit ugy lehet megtenni hogy szamon tartjuk a beumeretet es elosszuk annyival, es akkor megkapjuk 
	RECT rcDt;                                                                          // hany egyseg a conosle ablakunk
	HWND hDtWnd = ::GetForegroundWindow();
	HDC hDtDC = GetDC(hDtWnd);

	if (hDtDC && GetWindowRect(hDtWnd, &rcDt)) {
		//Create mem DC & bitmap
		szelesseg = (rcDt.right - rcDt.left) - 31;
		hosszusag = (rcDt.bottom - rcDt.top) - 54;
		szelesseg = szelesseg / x_betu_meret;
		hosszusag = hosszusag / y_betu_meret;
	}
}

void keret_alkotas(Szinek szine_szoveg, Szinek hatter_szine_szoveg, Szinek keret_szin, int x, int y, int magassag, KERET keret_tipus, string szoveg, int hosszusag) {
	char bal_also_sarok = 219;
	char bal_felso_sarok = 219;
	char jobb_also_sarok = 219;
	char jobb_felso_sarok = 219;
	char fuggoleges = 219;
	char vizszintes = 219;
	int x_visszaalito = x;
	int y_visszaalito = y;
	if (keret_tipus == szimpla) {
		bal_also_sarok = 192;
		bal_felso_sarok = 218;
		jobb_also_sarok = 217;
		jobb_felso_sarok = 191;
		fuggoleges = 179;
		vizszintes = 196;
	}
	if (keret_tipus == dupla) {
		bal_also_sarok = 200;
		bal_felso_sarok = 201;
		jobb_also_sarok = 188;
		jobb_felso_sarok = 187;
		fuggoleges = 186;
		vizszintes = 205;
	}
	if (keret_tipus == vastag) {
		bal_also_sarok = 254;
		bal_felso_sarok = 254;
		jobb_also_sarok = 254;
		jobb_felso_sarok = 254;
		fuggoleges = 219;
		vizszintes = 254;
	}
	if (szoveg != "") {
		hosszusag = szoveg.size();
	}
	helyez_kurzor(x, y);
	color(keret_szin);
	cout << bal_felso_sarok;
	for (int i = 0; i < hosszusag; i++) {
		cout << vizszintes;
	}
	cout << jobb_felso_sarok << endl;
	helyez_kurzor(x, ++y);
	for (int i = 0; i < magassag; i++) {
		if (i == magassag / 2) {
			cout << fuggoleges;
			color(szine_szoveg, hatter_szine_szoveg);
			if (szoveg != "") cout << szoveg;
			else x++;
			color(keret_szin);
			if (szoveg != "") x += szoveg.size() + 1;
			else {
				for (int j = 0; j < hosszusag; j++) {
					x++;
				}
			}
			helyez_kurzor(x, y);
			cout << fuggoleges;
			x = x_visszaalito;
			helyez_kurzor(x, ++y);
		}
		else {
			cout << fuggoleges;
			for (int i = 0; i < hosszusag; i++) {
				x++;
			}
			helyez_kurzor(++x, y);
			cout << fuggoleges;
			x = x_visszaalito;
			helyez_kurzor(x, ++y);
		}
	}
	cout << bal_also_sarok;
	for (int i = 0; i < hosszusag; i++) {
		cout << vizszintes;
	}
	cout << jobb_also_sarok << endl;
}

void keret_torles(string szoveg, int x, int y) {
	helyez_kurzor(x, y);
	cout << " ";
	for (int i = 0; i < szoveg.size(); i++) {
		cout << " ";
	}
	cout << " " << endl;
	helyez_kurzor(x, y + 1);
	cout << " " << szoveg << " " << endl;
	helyez_kurzor(x, y + 2);
	cout << " ";
	for (int i = 0; i < szoveg.size(); i++) {
		cout << " ";
	}
	cout << " " << endl;
}

void GetWindowPos(int& x, int& y) {
	RECT rect = { NULL };
	if (GetWindowRect(GetConsoleWindow(), &rect)) {
		x = rect.left;
		y = rect.top;
	}
}

POINT mouse_position(int x_meret, int y_meret) {
	POINT p;
	GetCursorPos(&p);
	int x_, y_;
	GetWindowPos(x_, y_);
	p.x = p.x - x_;
	p.y = p.y - y_;
	if (!fullscreen) {
		p.y -= 30;
		p.x -= 8;
	}

	p.y = p.y / y_meret;
	p.x = p.x / x_meret;


	return p;
}

int menu(vector<string> menu_cimek, string menu_iranya, int hely_x, int hely_y, Szinek menu_szine, Szinek menu_hatter_szine,
	bool keret, bool egerrel_valasztas, int y_meret, int x_meret, int sor_kozok) {
	int menuPointer = -1;
	int meret = menu_cimek.size() - 1;

	while (true) {
		if (menu_iranya == "column") {
			if (egerrel_valasztas == false) {
				if (GetAsyncKeyState(VK_DOWN) & 1) {
					menuPointer++;
					if (menuPointer > meret) {
						menuPointer = menu_cimek.size() - 1;
					}
				}
				if (GetAsyncKeyState(VK_UP) & 1) {
					menuPointer--;
					if (menuPointer < 0) {
						menuPointer = 0;
					}
				}
			}
			if (egerrel_valasztas == true) {
				POINT mouse_coord = mouse_position(x_meret, y_meret);
				helyez_kurzor(hely_x, hely_y);
				int elozo_helyzet_visszaalitasa = hely_y;
				if (keret == false) {
					for (int i = 0; i < menu_cimek.size(); i++) {
						int tulso_x_kordinata = hely_x + menu_cimek[i].size();
						if (mouse_coord.y == hely_y + i && mouse_coord.x > hely_x && mouse_coord.x < tulso_x_kordinata) {
							menuPointer = i;
							break;
						}
						else {
							menuPointer = -1;
						}
					}
				}
				if (keret == true) {
					for (int i = 0; i < menu_cimek.size(); i++) {
						int tulso_x_kordinata = hely_x + menu_cimek[i].size() + 4; // 4 azert kell hozzaadni, mert ra kell szamitani a keretet es a nagyobb betumeretet
						// hely_y + i + 3 ez az if-ben azert kell hogy vegye bele a keret magassagat s ne csak a szoveg magassagat
						if (mouse_coord.y > hely_y + i - 1 && mouse_coord.y < hely_y + i + 3 && mouse_coord.x > hely_x && mouse_coord.x < tulso_x_kordinata) {  // hely_y i - 1 a betumeret miatt, hanem nem kell
							menuPointer = i;
							break;
						}
						else {
							menuPointer = -1;
						}
						hely_y += sor_kozok + 2;
					}
					hely_y = elozo_helyzet_visszaalitasa;
				}
			}
			if (keret == false) {
				helyez_kurzor(hely_x, hely_y);
				for (int i = 0; i < menu_cimek.size(); i++) {
					helyez_kurzor(hely_x, hely_y + i);
					if (menuPointer == i) {
						color(menu_szine, menu_hatter_szine);
						cout << menu_cimek[i];
						color(white, black);
					}
					else {
						color(white, black);
						cout << menu_cimek[i] << endl;
					}
				}
			}
			if (keret == true) {
				int elozo_helyzet_visszaalitasa = hely_y;
				for (int i = 0; i < menu_cimek.size(); i++) {
					if (menuPointer == i) {
						keret_alkotas(menu_szine, menu_hatter_szine, white, hely_x, hely_y, 1, dupla, menu_cimek[i]);
					}
					else {
						keret_torles(menu_cimek[i], hely_x, hely_y);
						helyez_kurzor(hely_x + 1, hely_y + 1);
						cout << menu_cimek[i];
					}
					hely_y += sor_kozok + 3;
				}
				hely_y = elozo_helyzet_visszaalitasa;
			}
		}

		if (menu_iranya == "row") {
			if (egerrel_valasztas == false) {
				if (GetAsyncKeyState(VK_RIGHT) & 1) {
					menuPointer++;
					if (menuPointer > meret) {
						menuPointer = menu_cimek.size() - 1;
					}
				}
				if (GetAsyncKeyState(VK_LEFT) & 1) {
					menuPointer--;
					if (menuPointer < 0) {
						menuPointer = 0;
					}
				}
			}
			if (egerrel_valasztas == true) {
				POINT mouse_coord = mouse_position(x_meret, y_meret);
				helyez_kurzor(hely_x, hely_y);
				int elozo_helyzet_visszaalitasa = hely_x;
				if (keret == false) {
					for (int i = 0; i < menu_cimek.size(); i++) {
						int tulso_x_kordinata = hely_x + menu_cimek[i].size();
						if (mouse_coord.y == hely_y && mouse_coord.x >= hely_x && mouse_coord.x <= tulso_x_kordinata) {
							menuPointer = i;
							break;
						}
						else {
							menuPointer = -1;
						}
						hely_x += menu_cimek[i].size() + 1;
					}
					hely_x = elozo_helyzet_visszaalitasa;
				}
				if (keret == true) {
					for (int i = 0; i < menu_cimek.size(); i++) {
						int tulso_x_kordinata = hely_x + menu_cimek[i].size() + 1; //a keret miatt kell +1
						if (mouse_coord.y > hely_y - 1 && mouse_coord.y < hely_y + 3 && mouse_coord.x >= hely_x && mouse_coord.x <= tulso_x_kordinata) {
							menuPointer = i;
							break;
						}
						else {
							menuPointer = -1;
						}
						hely_x += menu_cimek[i].size() + 2;
					}
					hely_x = elozo_helyzet_visszaalitasa;
				}
			}
			if (keret == false) {
				helyez_kurzor(hely_x, hely_y);
				int elozo_helyzet_visszaalitasa = hely_x;
				for (int i = 0; i < menu_cimek.size(); i++) {
					if (menuPointer == i) {
						color(menu_szine, menu_hatter_szine);
						cout << menu_cimek[i];
						color(white, black);
					}
					else {
						color(white, black);
						cout << menu_cimek[i] << endl;
					}
					hely_x += menu_cimek[i].size() + 1;
					helyez_kurzor(hely_x, hely_y);
				}
				hely_x = elozo_helyzet_visszaalitasa;
			}
			if (keret == true) {
				int elozo_helyzet_visszaalitasa = hely_x;
				for (int i = 0; i < menu_cimek.size(); i++) {
					if (menuPointer == i) {
						keret_alkotas(menu_szine, menu_hatter_szine, white, hely_x, hely_y, 1, dupla, menu_cimek[i]);
					}
					else {
						keret_torles(menu_cimek[i], hely_x, hely_y);
						helyez_kurzor(hely_x + 1, hely_y + 1);
						cout << menu_cimek[i];
					}
					hely_x += menu_cimek[i].size() + 2;
				}
				hely_x = elozo_helyzet_visszaalitasa;
			}
		}

		if (GetAsyncKeyState(VK_RETURN) & 1 || GetAsyncKeyState(VK_LBUTTON) & 1) {
			for (int i = 0; i < menu_cimek.size(); i++) {
				if (menuPointer == i) {
					return menuPointer;
				}
			}
		}

	}
}

void console_window_size(int ablakhelye_x, int ablakhelye_y, int ablak_szelessege, int ablak_magassaga) {
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r); //tarolja a console jelenlegi mereteit

	MoveWindow(console, r.left, r.top, ablak_szelessege, ablak_magassaga, TRUE); // 800 width, 100 height
	SetWindowPos(console, 0, ablakhelye_x, ablakhelye_y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
}

void betumeret(int magassag, int szelesseg) {
	string Style[7];
	CONSOLE_FONT_INFOEX cfi;
	cfi.cbSize = sizeof(cfi);
	cfi.nFont = 0;
	cfi.dwFontSize.X = szelesseg;                   // szelesseg betunek
	cfi.dwFontSize.Y = magassag;                  // magassag betunek pixelben
	cfi.FontFamily = FF_DONTCARE;
	cfi.FontWeight = FW_NORMAL;
	//strcpy_s((char*)cfi.FaceName, 32, f); // betu tipust lehet allitani
	//wcscpy_s(cfi.FaceName, L"lucida console");
	SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);
}

bool gomb(int x, int y, int magassag, string szoveg, Szinek szoveg_szin, Szinek hatter_szin_szoveg, Szinek keret_szin, KERET keret_tipus, Szinek eger_rajta_van_szin, int hosszusag) {
	POINT p = mouse_position();
	helyez_kurzor(0, 0);
	int hatso_y = y + magassag + 1;
	int hatso_x = x + szoveg.size() + 1;
	//cout << "X: " << p.x << " Y: " << p.y << "     ";
	if (p.x >= x && p.x <= hatso_x && p.y >= y && p.y <= hatso_y) {
		keret_alkotas(szoveg_szin, hatter_szin_szoveg, eger_rajta_van_szin, x, y, magassag, keret_tipus, szoveg);
		return true;
	}
	else {
		keret_alkotas(szoveg_szin, hatter_szin_szoveg, keret_szin, x, y, magassag, keret_tipus, szoveg);
		return false;
	}

	//azert bool tipus mert ha az eger a gombon van akkor mar true erteket ad vissza igy konyebb volt vele programozni
}