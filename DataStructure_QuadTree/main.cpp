#include <iostream>
#include <fstream>
#include "QuadTree.h"
#include "menu.h"
#include <direct.h>  // _getcwd hez kell

#define S 3000

using namespace std;

void kiir(Csomopont* csomopont, int height, int width) {
	helyez_kurzor(csomopont->pozicio.x + width, height - csomopont->pozicio.y);
	cout << csomopont->adat.karakter;
}

QuadTree beolvas_kep_fajlbol(string eleresi_ut, int& row, int& column) {   //a kep megvan adva karakterekben, azokat beolvasom es hozzarendelek egy x y kordinatat
	ifstream fin(eleresi_ut);                                              //es lementem az adott karaktert arra a koordinatara
	column = 0;
	row = 1;
	char k = ' ';
	while (k != '\n') {   //megszamoljuk az oszlopok szamat
		fin.get(k);
		column++;
	}
	while (!fin.eof()) {     //megszamoljuk a sorok szamat
		string s;
		getline(fin, s);
		row++;
	}
	fin.close();
	QuadTree fa(-column, row, column, -row);

	fin.open(eleresi_ut);
	if (!fin.good()) throw "A fajl nem letezik!";   //a fin.good() ellenorzi, hogy letezike a fajl
	int y = row;
	for (int i = 0; i < row; i++) {
		int x = -column;
		for (int j = 0; j < column; j++) {
			Adatok pixel;
			fin.get(pixel.karakter);
			if (pixel.karakter != '\n') fa.beszuras(x, y, pixel);   //hogy ne olvassa be a sorvegi jelet, mert a fin.get() -beolvassa azt is,
			x++;                                                      //es akkor mindig berakna egy ujsort es elcsuszik a kep
		}
		y--;
	}
	fin.close();
	return fa;
}

QuadTree beolvas_adatok_fajlbol(string eleresi_ut, int& height, int& width) {   // a fajlban megvan adva egy x y koordinata es egy karakter ilyen formaban: x y karakter
	ifstream fin(eleresi_ut);
	if (!fin.good()) throw "A fajl nem letezik!";    //hibat valt ki ha nem letezik a fajl
	fin >> height >> width;                                    // a fajl elejen talalhato az hogy hanyszor hanyas a kep
	QuadTree fa(Pont(-width, height), Pont(width, -height));
	while (!fin.eof()) {
		Csomopont* csomopont = new Csomopont;
		int karakter_kod;
		fin >> csomopont->pozicio.x >> csomopont->pozicio.y >> karakter_kod;
		csomopont->adat.karakter = (char) karakter_kod;
		if (csomopont->adat.karakter != '\n') fa.beszuras(csomopont);
	}
	fin.close();
	return fa;
}

Pont beallit_kozepre(int x, int y, int merete_menunek) {    //menuhoz kell
	x /= 2;
	x -= 8;
	y /= 2;
	y -= merete_menunek + 3;
	return Pont(x, y);
}

void restoreWindow() {               // ujrameretezi az ablakot, mert kiraktam fullscreenre de ugy nem ment jol a kiiratas
	HWND console = GetConsoleWindow();
	SetWindowLong(console, GWL_STYLE, GetWindowLong(console, GWL_STYLE) | WS_OVERLAPPEDWINDOW);

	RECT windowRect = { 0, 0, 1800, 1000 }; //x y ablak elhelyezese saroktol nezve x y hossz ablak meret
	AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);
	SetWindowPos(console, NULL, 100, 100,         // beallitja az ablak helyzetet
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top, SWP_NOZORDER | SWP_NOOWNERZORDER);
}

vector<string> kepek_cime_beolvasas() {     // egy txt ben tarolom, hogy milyen kepek vannak a galeriaban
	ifstream fin("jatekhoz\\kep_cimek.txt");
	vector<string> cimek;
	while (!fin.eof()) {
		string sor;
		getline(fin, sor);
		if (sor != "\n" && sor != "") cimek.push_back(sor);  //azert kellett mert rakott be egy uj sort es csusztak el a gombok
	}
	return cimek;
}

void hozzaad_kepcimekhez(string kep_cime) {
	ofstream fout("jatekhoz\\kep_cimek.txt", ios_base::app);  //azert kell, hogy ne torolje ami benne van hanem additivan adja hozza
	fout << kep_cime << endl;
	fout.close();
}

void torles_kep_cimek_kozul(string cim) {    //beolvassa az osszes kep cimet es eltavolitja amelyikre nincs szukseg, es ujbol kiirja fajlba
	ifstream fin("jatekhoz\\kep_cimek.txt");
	vector<string> cimek;
	while (!fin.eof()) {
		string sor;
		fin >> sor;
		cimek.push_back(sor);
	}
	fin.close();
	vector<string>::iterator it;
	for (int i = 0; i < cimek.size(); i++) {
		if (cimek[i] == cim) it = cimek.begin() + i;  //vegig megyek es amelyiket kell eltavolitom
	}
	cimek.erase(it);

	ofstream fout("jatekhoz\\kep_cimek.txt");
	for (int i = 0; i < cimek.size(); i++) {    //ujbol kiirom fajlba
		fout << cimek[i] << endl;
	}
	fout.close();
}

void atnevez_kep(string regi_nev, string uj_nev) {  //beolvasom az osszes kep cimet es amelyknek kell kicserelem a nevet es ujbol kiirom fajlba
	ifstream fin("jatekhoz\\kep_cimek.txt");
	vector<string> cimek;
	while (!fin.eof()) {
		string sor;
		fin >> sor;
		cimek.push_back(sor);
	}
	fin.close();

	for (int i = 0; i < cimek.size(); i++) {
		if (cimek[i] == regi_nev) cimek[i] = uj_nev;
	}

	ofstream fout("jatekhoz\\kep_cimek.txt");
	for (int i = 0; i < cimek.size(); i++) {
		fout << cimek[i] << endl;
	}
	fout.close();

	regi_nev = "jatekhoz\\" + regi_nev;           // a mappaban levo fajlokat es atnevezem windows paranccsal
	string parancs = "ren " + regi_nev + " " + uj_nev;
	system(parancs.c_str());    // c stringre alakitja, mert ilyent var el a fuggveny
}

int main() {
	int betu_x = 25;
	int betu_y = 18;
	ShowScrollBar(GetConsoleWindow(), SB_VERT, 0);   // eltunteti a scroll bart fuggolegesen
	ShowScrollBar(GetConsoleWindow(), SB_HORZ, 0);   // eltunteti a scroll bart vizszintesen
	betumeret(betu_x, betu_y);                       // beallitja a console betumeretet
	restoreWindow();
	int x_hossz_kepernyo, y_hossz_kepernyo;         //ablak hosszusaga vizszintesen es fuggolegesen
	window_size(x_hossz_kepernyo, y_hossz_kepernyo, betu_x, betu_y);  //lekeri az ablak meretet

	vector<string> menu_cimek = { "   FOTOK   ", " FELTOLTES ", "KONVERTALAS", " ATNEVEZES ", "  TORLES   ", "   EXIT    " };   // menu cimek letrehozasa
	Pont fo_menu_kozep = beallit_kozepre(x_hossz_kepernyo, y_hossz_kepernyo, menu_cimek.size());   // elhelyezi kozepre
	int ertek;
	do {
		system("cls");
		ertek = menu(menu_cimek, "column", fo_menu_kozep.x, fo_menu_kozep.y, light_aqua, black, true, true, betu_x, betu_y);
		fflush(stdout);
		fflush(stdin);
		if (ertek == 0) {   //FOTO menupont
			system("cls");
			vector<string> foto_menu_cimek = kepek_cime_beolvasas();   // beolvassa az eltarolt fotok neveit
			foto_menu_cimek.push_back("VISSZA     ");                // hozzaadunk egy visszalepes gombot is
			Pont kozep = beallit_kozepre(x_hossz_kepernyo, y_hossz_kepernyo, foto_menu_cimek.size());
			int f_menu;
			do {
				system("cls");
				betumeret(betu_x, betu_y);
				f_menu = menu(foto_menu_cimek, "column", kozep.x, kozep.y, green, black, false, true, betu_x, betu_y);
				if (f_menu != foto_menu_cimek.size() - 1) {    // ha nem egyenlo az exit gombal
					helyez_kurzor(kozep.x - 8, kozep.y + 13);
					color(light_red);
					cout << "Varjal amig betolt a kep!";
					color(white);
					int height, width;
					string ut = "jatekhoz\\" + foto_menu_cimek[f_menu];    // a beolvasott kepek neveinek a helye
					QuadTree fa = beolvas_adatok_fajlbol(ut, height, width);  // eltarolom a quadfaban a beolvasott adatokat
					betumeret(2, 1);
					system("cls");
					restoreWindow();   //ujra kell meretezni az ablakot, mert hanem nem megy jol a kiiras, mert nem frissit a console ablak meretein
					fa.vegrehajt_elemeken(height, width, kiir);   // elvegzi a muveletet a quadfa minden csomopontjan
					system("pause");                           // varakozik amig a felhasznalo le nem nyom egy billentyut
				}
				fflush(stdin);
			} while (f_menu != foto_menu_cimek.size() - 1);   // ha VISSZA gomb akkor visszalep egyet
		}
		if (ertek == 1) {  // FELTOLTES
			system("cls");
			helyez_kurzor(0, 0);
			string eleresi_ut, nev;
			cout << "Add meg az eleresi utat: ";   //meg kell adni hol talalhato a feltolteni kivant kep
			cin >> eleresi_ut;
			fflush(stdin);
			cout << "Add meg a nevet: ";   // mi lesz az uj neve
			cin >> nev;
			fflush(stdin);
			string jelenlegi_konyvtar = _getcwd(0, 0);   // lekeri az aktualis konyvtar teljes eleresi utjat, amelyikben fut a kod
			jelenlegi_konyvtar += "\\jatekhoz\\";
			string parancs = "copy " + eleresi_ut + " " + jelenlegi_konyvtar + nev;  //letrehozom a windows commandot
			if (!system(parancs.c_str())) {   // ha nullat ad vissza akkor sikerult a parancs vegrehajtasa
				hozzaad_kepcimekhez(nev);       // hozzaadom a fajlhoz amelyikben tarolom a fotok neveit
				color(green);
				cout << "A fajl sikeresen feltoltve a galeriaba!" << endl;
				color(white);
				Sleep(S);
			}
			else {
				color(red);
				cout << "A fajl nem letezik!" << endl;
				Sleep(S);
				color(white);
			}
		}
		if (ertek == 2) {  // KONVERTALAS   atalakitja egy ascii karakterekben megadott fajlt x y adat rendszerbe
			system("cls");
			string utvonal, nev;
			cout << "Add meg az eleresi utvanalat a fajlnak amit akarsz konvertalni: " << endl;
			getline(cin, utvonal);
			cout << "Varjal amig befejezodik a konvertalas!" << endl;
			try {
				int height = 0, width = 0;
				QuadTree fa = beolvas_kep_fajlbol(utvonal, height, width);  // eltarolom a quadfaban a beolvasott adatokat
				fa.exportal_adatok_fajlba(utvonal, height, width);   // kimentem fajlba a quadfa tartalmat x y adat rendszerben
				color(green);
				cout << "A fajl sikeresen konvertalva!";
				Sleep(S);
				color(white);
			}
			catch (const char* s) {
				cout << s;
			}
		}
		if (ertek == 3) {  // ATNEVEZES  atnevezi a megadott kepet, kicsereli a txt ben a nevet
			system("cls");
			string kep, ujnev;
			cout << "Melyik kepet szeretned atnevezni: ";
			cin >> kep;
			cout << "Mi legyen az uj neve: ";
			cin >> ujnev;
			atnevez_kep(kep, ujnev);
			color(green);
			cout << "A kep sikeresen atnevezve!";
			Sleep(S);
			color(white);
		}
		if (ertek == 4) {  // TORLES  torli a megadott kepet
			string cim;
			system("cls");
			cin.ignore();
			cout << "Melyik kepet szeretned torolni: ";
			cin >> cim;
			torles_kep_cimek_kozul(cim);
			color(green);
			cout << "Sikeresen torolted a fotot" << endl;
			Sleep(S);
			color(white);
		}
		fflush(stdout);
		fflush(stdin);
	} while (ertek != menu_cimek.size() - 1);
	
	return 0;
}
