#include "QuadTree.h"
#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

Pont::Pont() {
	x = 0;
	y = 0;
}

Pont::Pont(int x, int y) {
	this->x = x;
	this->y = y;
}

Adatok::Adatok() {
	karakter = ' ';    //ha nem adok meg semmit akkor space lesz az alapertelmezett karakter
	szin = 0;
	hatter_szin = 0;
}

Adatok::Adatok(char karakter) {
	this->karakter = karakter;
	szin = 0;
	hatter_szin = 0;
}

Adatok::Adatok(char karakter, int szin, int hatter_szin) {
	this->karakter = karakter;
	this->szin = szin;
	this->hatter_szin = hatter_szin;
}

Csomopont::Csomopont() {
	adat.karakter = NULL;
	adat.szin = NULL;
	adat.hatter_szin = NULL;
}

Csomopont::Csomopont(int x, int y, Adatok adat) {
	pozicio.x = x;
	pozicio.y = y;
	this->adat = adat;  //ez lehet ascii kod, vagy szin akar
}

QuadTree::QuadTree() {  //alapertelmezett konstruktor
	bal_felso_fa = NULL;
	jobb_felso_fa = NULL;   //inicializaljuk a fa minden gyermeket NULL-al
	bal_also_fa = NULL;     //minden fa mutat NULL-ba
	jobb_also_fa = NULL;
	csucs = NULL;
	bal_felso = Pont(1, 1);  //inicializaljuk a csomopont hatarait, mivel egy negyzet es x y kordinata rendszerben vagyunk
	jobb_also = Pont(1, -1);  //legalabb 4 pixel, kocka, vagy valamilyen egyseg, kell legyen, hogy lehessen felosztani 4 fele
}                             //az euklideszi tavolsag legyen legalabb 2 a ket sarok kozott

QuadTree::QuadTree(int bal_felso_x, int bal_felso_y, int jobb_also_x, int jobb_also_y) {   //ertekekkel inicializalo konstruktor
	bal_felso.x = bal_felso_x;
	bal_felso.y = bal_felso_y;
	jobb_also.x = jobb_also_x;
	jobb_also.y = jobb_also_y;
	csucs = NULL;
	bal_felso_fa = NULL;
	jobb_felso_fa = NULL;
	bal_also_fa = NULL;
	jobb_also_fa = NULL;
}

QuadTree::QuadTree(Pont bal_felso, Pont jobb_also) {   //Pont tipusu structuraval inicializalo konstruktor
	this->bal_felso = bal_felso;
	this->jobb_also = jobb_also;
	bal_felso_fa = NULL;
	jobb_felso_fa = NULL;
	bal_also_fa = NULL;
	jobb_also_fa = NULL;
	csucs = NULL;
}

QuadTree::~QuadTree() {   //destruktor
	delete bal_felso_fa;   //eleg csak meghivni egyszeruen delete, mert egy QuadFa a QuadFaban es, ha meghivok egy deletet, akkor
	delete jobb_felso_fa;  //meghivja arra a QuadFara megegyszer a destruktort es ezt a hivogatast addig vegzi amig le nem er
	delete bal_also_fa;    //az utolso QuadFaig es akkor azt torli, olyan mint egy rekurzio
	delete jobb_also_fa;
}

void QuadTree::beszuras(Csomopont* ujpont) {
	//hibaellenorzes
	if (ujpont == NULL) return;  //ha NULL a csomopont akkor ne szurja be, pl Csomopont *a = NULL; csak letezo csomopontot lehet beszurni
	if (!hataron_belul_van(ujpont->pozicio)) throw "A beszurando elem hataron kivul van!";

	//ha leertunk 1 egysegig(ez lehet pixel, vagy akar mi), akkor beszurjuk a pontot
	if (abs(bal_felso.x - jobb_also.x) <= 1 && abs(bal_felso.y - jobb_also.y) <= 1) {
		if (csucs == NULL) csucs = ujpont;
		return;
	}

	//osszeadom a ket sarkat es mindig osztom 2 fele es akkor megkapom, hogy a pont jobb vagy bal oldalt van, vagy fent vagy lent van
	//ha x koordinatakat adom ossze es osztom el akkor megkapom, hogy jobb vagy bal oldalt van a pont
	//ha y koordinatakat adom ossze es osztom el akkor megkapom, hogy fent vagy lent van a pont
	if (((bal_felso.x + jobb_also.x) / 2) > ujpont->pozicio.x) { //megnezem jobb vagy bal oldalt vane, ez itt bal oldal ha teljesul
		//bal felso QuadFa
		if (((bal_felso.y + jobb_also.y) / 2) < ujpont->pozicio.y) {   //megnezem alul vagy felul vane, itt felul lesz ha teljesul if
			if (bal_felso_fa == NULL) bal_felso_fa = new QuadTree(Pont(bal_felso.x, bal_felso.y), Pont(((bal_felso.x + jobb_also.x) / 2), ((bal_felso.y + jobb_also.y) / 2)));
			bal_felso_fa->beszuras(ujpont);
		}
		else {  //bal also QuadFa
			if (bal_also_fa == NULL) bal_also_fa = new QuadTree(Pont(bal_felso.x, ((bal_felso.y + jobb_also.y) / 2)), Pont(((bal_felso.x + jobb_also.x) / 2), jobb_also.y));
			bal_also_fa->beszuras(ujpont);
		}
	}
	else {   //jobb oldal
		//jobb felso QuadFa
		if (((bal_felso.y + jobb_also.y) / 2) < ujpont->pozicio.y) {
			if (jobb_felso_fa == NULL) jobb_felso_fa = new QuadTree(Pont((bal_felso.x + jobb_also.x) / 2, bal_felso.y), Pont(jobb_also.x, (bal_felso.y + jobb_also.y) / 2));
			jobb_felso_fa->beszuras(ujpont);
		}
		else {  //jobb also QuadFa
			if (jobb_also_fa == NULL) jobb_also_fa = new QuadTree(Pont((bal_felso.x + jobb_also.x) / 2, (bal_felso.y + jobb_also.y) / 2), Pont(jobb_also.x, jobb_also.y));
			jobb_also_fa->beszuras(ujpont);
		}
	}
}

void QuadTree::beszuras(int x, int y, Adatok adat) {  //megirtam a beszurast alap tipusokra is, hogy ne csak Csomopont strukturaval menjen
	Csomopont* csomopont = new Csomopont;
	csomopont->pozicio.x = x;
	csomopont->pozicio.y = y;
	csomopont->adat = adat;
	beszuras(csomopont);
}

Csomopont* QuadTree::kereses(Pont keresett_pont) {  // megkeresi a parameterkent megadott koordinatat
	//hibaellenorzes
	if (!hataron_belul_van(keresett_pont)) throw "A keresett elem hataron kivul esik!";
	if (csucs != NULL) return csucs;

	//kereses
	if ((bal_felso.x + jobb_also.x) / 2 > keresett_pont.x) {
		//bal felso
		if ((bal_felso.y + jobb_also.y) / 2 < keresett_pont.y) {
			if (bal_felso_fa == NULL) return NULL;
			else return bal_felso_fa->kereses(keresett_pont);
		}
		else {  //bal also fa
			if (bal_also_fa == NULL) return NULL;
			else return bal_also_fa->kereses(keresett_pont);
		}
	}
	else {   //jobb oldal
		//jobb felso
		if (((bal_felso.y + jobb_also.y) / 2) < keresett_pont.y) {
			if (jobb_felso_fa == NULL) return NULL;
			else return jobb_felso_fa->kereses(keresett_pont);
		}
		else {  //jobb also
			if (jobb_also_fa == NULL) return NULL;
			else return jobb_also_fa->kereses(keresett_pont);
		}
	}
}

Csomopont* QuadTree::kereses(int x, int y) {  // ugyanaz csak nem Pont tipusu strukturaval hanem intel mukodik
	return kereses(Pont(x, y));
}

bool QuadTree::hataron_belul_van(Pont pont) {   //ezzel a fuggvennyel ellenorzom hogy hataron kivuli indexet kapott-e
	if (pont.x <= jobb_also.x && pont.x >= bal_felso.x && pont.y >= jobb_also.y && pont.y <= bal_felso.y) return true;
	else return false;
}

bool QuadTree::hataron_belul_van(int x, int y) {
	return hataron_belul_van(Pont(x, y));
}

void QuadTree::torles(Pont pont) {
	//hibaellenorzes
	if (!hataron_belul_van(pont)) throw "A keresett elem hataron kivul esik!";  //ha kiindexel a felhasznalo akkor kivetelt valtok ki  
	if (csucs != NULL) {                //a hatart a bal_felso es jobb_also valtozok szabjak meg
		csucs = NULL;             //ha nem null egy csomopont csak akkor kerul torlesre
		bal_felso.x = NULL;
		bal_felso.y = NULL;
		jobb_also.x = NULL;
		jobb_also.y = NULL;
		delete csucs;
		delete bal_felso_fa;
		delete jobb_felso_fa;
		delete bal_also_fa;
		delete jobb_also_fa;
		return;
	}
	//minden_ag_null fuggvenyt arra hasznalom hogy, amit mar toroltem csomopontot az oda vezeto utat NULLazzam, hogy tobbet ne
	// lehessen oda elmenni, mert hanem le lehet menni es kiolvasodik egy gepszemet ami delete utan bekerul a memoriaba 
	//kereses
	if ((bal_felso.x + jobb_also.x) / 2 > pont.x) {
		//bal felso
		if ((bal_felso.y + jobb_also.y) / 2 < pont.y) {
			if (bal_felso_fa != NULL) {
				bal_felso_fa->torles(pont);
				if (minden_ag_null(bal_felso_fa)) bal_felso_fa = NULL;
			}
		}
		else {  //bal also fa
			if (bal_also_fa != NULL) {
				bal_also_fa->torles(pont);
				if (minden_ag_null(bal_also_fa)) bal_also_fa = NULL;
			}
		}       // ha nem talalhato a keresett elem akkor, csak siman nem teljesul a feltetel es nem tortenik semmi
	}
	else {   //jobb oldal
		//jobb felso
		if (((bal_felso.y + jobb_also.y) / 2) < pont.y) {
			if (jobb_felso_fa != NULL) {
				jobb_felso_fa->torles(pont);
				if (minden_ag_null(jobb_felso_fa)) jobb_felso_fa = NULL;
			}
			else {  //jobb also
				if (jobb_also_fa != NULL) {
					jobb_also_fa->torles(pont);
					if (minden_ag_null(jobb_also_fa)) jobb_also_fa = NULL;
				}
			}
		}
	}
}

void QuadTree::torles(int x, int y) {
	torles(Pont(x, y));
}

bool QuadTree::minden_ag_null(QuadTree* fa) {
	return fa->bal_felso_fa == NULL && fa->jobb_felso_fa == NULL && fa->bal_also_fa == NULL && fa->jobb_also_fa == NULL && fa->csucs == NULL;
}

vector<Csomopont> QuadTree::lekerdezes(Pont bal_felso, Pont jobb_also) {  //visszateriti a csomopontokat egy adott tartomanyban
	vector<Csomopont> eredmeny;
	if (hataron_belul_van(bal_felso) && hataron_belul_van(jobb_also)) {
		if (bal_felso_fa != NULL) lekerdez_reszfa(bal_felso_fa, bal_felso, jobb_also, eredmeny);  //azert kell != NULL hogy ne mutassak
		if (jobb_felso_fa != NULL) lekerdez_reszfa(jobb_felso_fa, bal_felso, jobb_also, eredmeny);  //egy nem letezo fara
		if (bal_also_fa != NULL) lekerdez_reszfa(bal_also_fa, bal_felso, jobb_also, eredmeny);
		if (jobb_also_fa != NULL) lekerdez_reszfa(jobb_also_fa, bal_felso, jobb_also, eredmeny);
	}
	else throw "A lefedes amit megadtal a letezo teruleten kivul esik!";

	return eredmeny;
}

void QuadTree::lekerdez_reszfa(QuadTree* fa, Pont bal_felso, Pont jobb_also, vector<Csomopont>& eredmeny) {
	//rekurzivan addig megy le amig a csucs != NULL es eltarolja egy vektorba es a vegen visszateriti
	if (!hataron_belul_van(bal_felso) || !hataron_belul_van(jobb_also)) return;  //ha a megadott lefedes nincs az adott Quadfa hataran belul akkor mar nem keresek ott csomopontot
	if (fa->csucs != NULL && fa->csucs->pozicio.x >= bal_felso.x && fa->csucs->pozicio.x <= jobb_also.x && fa->csucs->pozicio.y <= bal_felso.y && fa->csucs->pozicio.y >= jobb_also.y) {
		Csomopont csomo_p(fa->csucs->pozicio.x, fa->csucs->pozicio.y, fa->csucs->adat);
		eredmeny.push_back(csomo_p);
		return;
	}
	if (fa->bal_felso_fa != NULL) lekerdez_reszfa(fa->bal_felso_fa, bal_felso, jobb_also, eredmeny);
	if (fa->jobb_felso_fa != NULL) lekerdez_reszfa(fa->jobb_felso_fa, bal_felso, jobb_also, eredmeny);  // meghivjuk mind a negy utodra
	if (fa->bal_also_fa != NULL) lekerdez_reszfa(fa->bal_also_fa, bal_felso, jobb_also, eredmeny);
	if (fa->jobb_also_fa != NULL) lekerdez_reszfa(fa->jobb_also_fa, bal_felso, jobb_also, eredmeny);
}

vector<Csomopont> QuadTree::lekerdezes(int x1, int y1, int x2, int y2) {
	return lekerdezes(Pont(x1, y1), Pont(x2, y2));
}

void QuadTree::kiir_elemek_utvonallal() {
	vector<string> utvonal;        //kiirja utvonallal egyutt a csomopontot
	if (bal_felso_fa != NULL) {
		utvonal.push_back("bal_felso_fa --> ");   //utvonal alatt az oda vezeto lepeseket ertem
		kiir_reszfa(bal_felso_fa, utvonal);
		utvonal.pop_back();
	}
	if (jobb_felso_fa != NULL) {
		utvonal.push_back("jobb_felso_fa --> ");
		kiir_reszfa(jobb_felso_fa, utvonal);
		utvonal.pop_back();
	}
	if (bal_also_fa != NULL) {
		utvonal.push_back("bal_also_fa --> ");
		kiir_reszfa(bal_also_fa, utvonal);
		utvonal.pop_back();
	}
	if (jobb_also_fa != NULL) {
		utvonal.push_back("jobb_also_fa --> ");
		kiir_reszfa(jobb_also_fa, utvonal);
		utvonal.pop_back();
	}
}

void QuadTree::kiir_reszfa(QuadTree* fa, vector<string> utvonal) {  // az utvonalas kiirashoz van szukseg 
	if (fa->csucs != NULL) {
		for (int i = 0; i < utvonal.size(); i++) cout << utvonal[i];
		cout << fa->csucs->pozicio.x << " " << fa->csucs->pozicio.y << " " << fa->csucs->adat.karakter << endl;
		return;
	}
	if (fa->bal_felso_fa != NULL) {
		utvonal.push_back("bal_felso_fa --> ");   // rekurzivan adja hozza ahogyan megy le a faban
		kiir_reszfa(fa->bal_felso_fa, utvonal);
		utvonal.pop_back();
	}
	if (fa->jobb_felso_fa != NULL) {
		utvonal.push_back("jobb_felso_fa --> ");   // hozzaadja amelyik agon megy lefele
		kiir_reszfa(fa->jobb_felso_fa, utvonal);
		utvonal.pop_back();
	}
	if (fa->bal_also_fa != NULL) {
		utvonal.push_back("bal_also_fa --> ");
		kiir_reszfa(fa->bal_also_fa, utvonal);
		utvonal.pop_back();
	}
	if (fa->jobb_also_fa != NULL) {
		utvonal.push_back("jobb_also_fa --> ");
		kiir_reszfa(fa->jobb_also_fa, utvonal);
		utvonal.pop_back();
	}
}

void QuadTree::vegrehajt_elemeken(int height, int width, void(*fuggveny)(Csomopont* elem, int magassag, int szelesseg)) {
	if (bal_felso_fa != NULL) vegrehajt_reszfan(bal_felso_fa, height, width, fuggveny);   //itt mindengol a height, width, magassag, szelesseg csak azert kellett, hogy a kiiro fuggvenyemnek
	if (jobb_felso_fa != NULL) vegrehajt_reszfan(jobb_felso_fa, height, width, fuggveny);  //attudjam adni a beolvasott kep mereteit, mert ki kell vonjon belole es az szerint helyezi a kurzort
	if (bal_also_fa != NULL) vegrehajt_reszfan(bal_also_fa, height, width, fuggveny);
	if (jobb_also_fa != NULL) vegrehajt_reszfan(jobb_also_fa, height, width, fuggveny);
}

void QuadTree::vegrehajt_reszfan(QuadTree* fa, int height, int width, void(*fuggveny)(Csomopont* elem, int magassag, int szelesseg)) {
	if (fa->csucs != NULL) {
		fuggveny(fa->csucs, height, width);
		return;
	}
	if (fa->bal_felso_fa != NULL) vegrehajt_reszfan(fa->bal_felso_fa, height, width, fuggveny);
	if (fa->jobb_felso_fa != NULL) vegrehajt_reszfan(fa->jobb_felso_fa, height, width, fuggveny);
	if (fa->bal_also_fa != NULL) vegrehajt_reszfan(fa->bal_also_fa, height, width, fuggveny);
	if (fa->jobb_also_fa != NULL) vegrehajt_reszfan(fa->jobb_also_fa, height, width, fuggveny);
}

void QuadTree::kiir_fajlba(ofstream& fout) {
	if (csucs != NULL) fout << csucs->pozicio.x << " " << csucs->pozicio.y << " " << (int) csucs->adat.karakter << endl;
	if (bal_felso_fa != NULL) bal_felso_fa->kiir_fajlba(fout);       //itt a 337 es sorban azert kellett int kent beirni, mert mikor olvastam ki a fajlbol akkor a spacet nem vette be
	if (jobb_felso_fa != NULL) jobb_felso_fa->kiir_fajlba(fout);    //es igy az ascii kodja kerul a fajlba es utann atalakitom karakterre a beolvasasnel
	if (bal_also_fa != NULL) bal_also_fa->kiir_fajlba(fout);
	if (jobb_also_fa != NULL) jobb_also_fa->kiir_fajlba(fout);

}

void QuadTree::exportal_adatok_fajlba(string eleresi_ut, int height, int width) {
	ofstream fout(eleresi_ut);
	fout << height << " " << width << endl;
	kiir_fajlba(fout);
	fout.close();
}

void QuadTree::set_hatarok(Pont bal_felso, Pont jobb_also) {
	if (helyesek_hatarok(bal_felso, jobb_also)) {
		this->bal_felso = bal_felso;
		this->jobb_also = jobb_also;
	}
}

void QuadTree::set_hatarok(int bal_felso_x, int bal_felso_y, int jobb_also_x, int jobb_also_y) {
	set_hatarok(Pont(bal_felso_x, bal_felso_y), Pont(jobb_also_x, jobb_also_y));
}

bool QuadTree::helyesek_hatarok(Pont bal_felso, Pont jobb_also) {   // az euklideszi tavolsag azert kell legalabb ketto legyen mert akkor a quadfa feloszthato 4 fele
	int euklideszi_tavolsag = sqrt((pow((bal_felso.x - jobb_also.x), 2) + pow((bal_felso.y - jobb_also.y), 2)));
	if (euklideszi_tavolsag >= 2) {
		return true;
	}
	else throw "Ezek az ertekek nem allithatoak be a QuadFa hatarainak, legalabb 2 kell legyen koztuk a tavolsag!";
}
