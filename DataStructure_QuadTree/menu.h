#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <conio.h>
#include <stdio.h>
#include <Windows.h>
#include <functional>

using namespace std;

enum Szinek {
	black, blue, green, aqua, red, purple, yellow, white, gray, light_blue, light_green, light_aqua, light_red, light_purple, light_yellow, bright_white
};

enum KERET {
	szimpla, dupla, vastag
};

template<typename T>
void color(T szoveg_szin = white, T hatter_szin = black);
void helyez_kurzor(int x, int y);
void size_screenBuffer(int& oszlopok_szama_X, int& sorok_szama_Y);
void cursor_pos_screenBuffer(int& x, int& y);
void window_size(int& szelesseg, int& hosszusag, int y_betu_meret = 16, int x_betu_meret = 8);
void keret_alkotas(Szinek szine_szoveg, Szinek hatter_szine_szoveg, Szinek keret_szin, int x, int y, int magassag, KERET keret_tipus, string szoveg = "", int hosszusag = 0);
void keret_torles(string szoveg, int x, int y);
void GetWindowPos(int& x, int& y);
POINT mouse_position(int x_meret = 8, int y_meret = 16);
int menu(vector<string> menu_cimek, string menu_iranya, int hely_x, int hely_y, Szinek menu_szine, Szinek menu_hatter_szine, bool keret = false, bool egerrel_valasztas = false, int y_meret = 16, int x_meret = 8, int sor_kozok = 0);
void console_window_size(int ablakhelye_x, int ablakhelye_y, int ablak_szelessege, int ablak_magassaga);
void betumeret(int magassag, int szelesseg);
bool gomb(int x, int y, int magassag, string szoveg, Szinek szoveg_szin = red, Szinek hatter_szin_szoveg = black, Szinek keret_szin = yellow, KERET keret_tipus = dupla, Szinek eger_rajta_van_szin = blue, int hosszusag = 0);

#endif
