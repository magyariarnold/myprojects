#ifndef QUADTREE_H_INCLUDED
#define QUADTREE_H_INCLUDED
#include <vector>
#include <string>

using namespace std;

struct Pont {
	int x, y;
	Pont();
	Pont(int x, int y);
};

struct Adatok {
	char karakter;
	int szin;
	int hatter_szin;
	Adatok();
	Adatok(char karakter);
	Adatok(char karakter, int szin, int hatter_szin);
};

struct Csomopont {
	Adatok adat;
	Pont pozicio;
	Csomopont();
	Csomopont(int x, int y, Adatok adat);
};

class QuadTree {
	Pont bal_felso;
	Pont jobb_also;  //ezzel a ket valtozoval szamon tartom a hatarait az adott QuadFanak

	Csomopont* csucs;

	QuadTree* bal_felso_fa;  //a negy gyermek letrehozasa
	QuadTree* jobb_felso_fa;  //egybol ramutatok a negy gyermekre, nincs meg egy kulon mutato a gyokerre
	QuadTree* bal_also_fa;   //hanem alapertelmezetten azon vagyok es onnan mutatok a gyermekekre
	QuadTree* jobb_also_fa;

	bool hataron_belul_van(Pont pont);
	bool hataron_belul_van(int x, int y);
	bool minden_ag_null(QuadTree* fa);
	void lekerdez_reszfa(QuadTree* fa, Pont bal_felso, Pont jobb_also, vector<Csomopont>& eredmeny);
	void kiir_reszfa(QuadTree* fa, vector<string> utvonal);
	void vegrehajt_reszfan(QuadTree* fa, int height, int width, void(*fuggveny)(Csomopont* elem, int magassag, int szelesseg));
	bool helyesek_hatarok(Pont bal_felso, Pont jobb_also);
	void kiir_fajlba(ofstream& fout);
public:
	QuadTree();
	QuadTree(int bal_felso_x, int bal_felso_y, int jobb_also_x, int jobb_also_y);
	QuadTree(Pont bal_felso, Pont jobb_also);
	~QuadTree();
	void beszuras(Csomopont* ujpont);
	void beszuras(int x, int y, Adatok adat);
	void torles(Pont pont);
	void torles(int x, int y);
	vector<Csomopont> lekerdezes(Pont bal_felso, Pont jobb_also);
	vector<Csomopont> lekerdezes(int x1, int y1, int x2, int y2);
	Csomopont* kereses(Pont keresett_pont);
	Csomopont* kereses(int x, int y);
	void kiir_elemek_utvonallal();
	void vegrehajt_elemeken(int height, int width, void(*fuggveny)(Csomopont* elem, int szelesseg, int magassag));
	void exportal_adatok_fajlba(string eleresi_ut, int height, int width);
	void set_hatarok(Pont bal_felso, Pont jobb_also);
	void set_hatarok(int bal_felso_x, int bal_felso_y, int jobb_also_x, int jobb_also_y);

};

#endif
