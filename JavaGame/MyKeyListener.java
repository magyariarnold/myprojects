import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MyKeyListener implements KeyListener {

    private Snake snake;
    private char previousDirection;

    public MyKeyListener(Snake snake) {
        this.snake = snake;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                if (!snake.isPause()) {
                    if (snake.getDirection() != 'R') {  // if azert kell hogy ellenkezo iranyba ne tudjon ra menni
                        snake.setDirection('L');        // a testere
                    }
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (!snake.isPause()) {
                    if (snake.getDirection() != 'L') {
                        snake.setDirection('R');
                    }
                }
                break;
            case KeyEvent.VK_DOWN:
                if (!snake.isPause()) {
                    if (snake.getDirection() != 'U') {
                        snake.setDirection('D');
                    }
                }
                break;
            case KeyEvent.VK_UP:
                if (!snake.isPause()) {
                    if (snake.getDirection() != 'D') {
                        snake.setDirection('U');
                    }
                }
                break;
            case KeyEvent.VK_P:
                if (snake.getDirection() != 'P') {
                    previousDirection = snake.getDirection();  // ha nem P akkor megjegyezzuk merre ment a kigyo
                    snake.setDirection('P');
                    snake.setPause(true);
                }
                else if (snake.getDirection() == 'P') {
                    snake.setPause(false);
                    snake.setDirection(previousDirection); // ha P az elozo irany es megint P-t nyom a felhasznalo akkor engedjuk a kigyot amerre ment
                }
                break;
            case KeyEvent.VK_S:
                previousDirection = snake.getDirection();
                snake.setPause(true);  // itt kell allitani a pauset mert ha a controlban case: 'S' ben allittom addig lefut a switch feletti for es a fejere masolja a testet es a checkCollisonOnBody bejelez es GameOver
                snake.setDirection('S');
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
