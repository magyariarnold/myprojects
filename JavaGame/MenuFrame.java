import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MenuFrame extends JFrame {

    private final int screenWidth, screenHeight;
    private boolean onMenuFrame = true;

    public MenuFrame(int x, int y, int screenWidth, int screenHeight, Snake snake, View view, Controller controller) {
        JFrame frame = this;
        SettingsFrame settingsFrame = new SettingsFrame(x, y, screenWidth, screenHeight, snake, view, controller, frame);  // letrehozom a beallitas ablakot hogy elvegezze a szukseges beallitasokat
        settingsFrame.setVisible(false);
        ScoresFrame scoresFrame = new ScoresFrame(x, y, screenWidth, screenHeight, frame, snake);
        scoresFrame.setVisible(false);
        final Dimension buttonSize = new Dimension(200, 40);
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        SoundPlayer menuSound = new SoundPlayer("src/sounds/sound1.wav");
        menuSound.startContinuousSoundPlayer();

        setBounds(x, y, screenWidth, screenHeight);  // a szelesseghez es magassaghoz hozzakellett adni hogy legyen kicsit nagyobb az ablak hogy jojjenek ki a kockak
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);  // sajat fuggvenyem en irtam
        setResizable(false);
        setTitle("Menu");

        // kigyos hatter beallitasa
        ImageIcon backgroundImage = new ImageIcon("src/images/snake2.png");
        BackgroundPanel backgroundPanel = new BackgroundPanel(backgroundImage);

        // Gombok
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setBounds((getWidth() - 1200) / 2, 700, 1200, 500);
        buttonsPanel.setOpaque(false);  // atlatszora allitjuk a gombos panelt
        JButton startButton = new JButton("Start Game");
        JButton settingsButton = new JButton("Settings");
        JButton scoresButton = new JButton("Scores");
        JButton exitButton = new JButton("Exit");
        startButton.setPreferredSize(buttonSize);
        settingsButton.setPreferredSize(buttonSize);
        scoresButton.setPreferredSize(buttonSize);
        exitButton.setPreferredSize(buttonSize);
        buttonsPanel.add(startButton);
        buttonsPanel.add(settingsButton);
        buttonsPanel.add(scoresButton);
        buttonsPanel.add(exitButton);
        setContentPane(buttonsPanel);

        // azert kell hogy a ket panelt egymasra tehessem igy, a hatterkepen lesznek a gombok
        JLayeredPane layeredPane = new JLayeredPane();
        layeredPane.add(backgroundPanel, Integer.valueOf(0));
        layeredPane.add(buttonsPanel, Integer.valueOf(1));
        setContentPane(layeredPane);

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                menuSound.stop();
                GameFrame gameFrame = new GameFrame(view, controller);
                gameFrame.setLocationRelativeTo(null);
            }
        });

        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                settingsFrame.setVisible(true);

            }
        });

        scoresButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                scoresFrame.setVisible(true);
            }
        });

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        setVisible(true);
    }

    public boolean getMenuState() {
        return onMenuFrame;
    }

    // egy private class a panel hatterkepenek beallitasaert van szukszeg erre
    private class BackgroundPanel extends JPanel {
        private Image backgroundImage;

        public BackgroundPanel(ImageIcon backgroundImage) {
            this.backgroundImage = backgroundImage.getImage();
            setBounds(0, 0, screenWidth, screenHeight);  // ez itt azert kellett mert hanem nem megy hatar allitas nelkul hogy a ket panelt egymasra helyezni
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (backgroundImage != null) {
                g.drawImage(backgroundImage, 0, 0, getWidth(), getHeight(), this);
            }
        }
    }
}
