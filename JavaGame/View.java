import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class View extends JPanel {

    final int ScreenWidth;
    final int ScreenHight;
    private final int UNIT_SIZE = 25;

    private final Snake snake;

    public View(Snake snake, int screenWidth, int screenHight) {
        this.snake = snake;
        this.ScreenWidth = screenWidth;
        this.ScreenHight = screenHight;
        setBackground(Color.black);
        setFocusable(true);  // kell hogy a felhasznalo gombnyomasai mukodjenek
        addKeyListener(new MyKeyListener(snake));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        draw(g);
    }

    public void draw(Graphics g) {
        // racsok kirajzolasa
        for (int i = 0; i <= ScreenWidth / UNIT_SIZE; i++) {
            g.drawLine(i*UNIT_SIZE, 0, i*UNIT_SIZE, ScreenHight);  // fuggoleges vonalak
        }
        for (int i = 0; i <= ScreenHight / UNIT_SIZE; i++) {
            g.drawLine(0, i*UNIT_SIZE, ScreenWidth, i*UNIT_SIZE);  // vizszintes vonalak
        }


        // alma kirajzolasa
        g.setColor(Color.red);
        g.fillOval(snake.getAppleX(), snake.getAppleY(), UNIT_SIZE, UNIT_SIZE);

        for (int i = 0; i < snake.getBodyParts(); i++) {
            if (i == 0) {  // kigyo fej kirajzolasa
                g.setColor(snake.getHeadColor());
                g.fillOval(snake.getX(i), snake.getY(i), UNIT_SIZE, UNIT_SIZE);
            } else {   // kigyo test kirajzolasa
                g.setColor(snake.getSnakeColor());
                g.fillOval(snake.getX(i), snake.getY(i), UNIT_SIZE, UNIT_SIZE);
            }
        }

        // Pontszamok
        g.setColor(Color.red);
        g.setFont(new Font("Ink Free", Font.BOLD, 40));
        FontMetrics metrics2 = g.getFontMetrics(g.getFont());
        g.drawString("Score: " + snake.getApplesEaten(), (ScreenWidth - metrics2.stringWidth(("Score: " + snake.getApplesEaten()))) / 2, g.getFont().getSize());

        if (snake.isGameOver()) {
            // GameOver szoveg
            g.setColor(Color.red);
            g.setFont(new Font("Ink Free", Font.BOLD, 75));
            FontMetrics metrics = g.getFontMetrics(g.getFont());
            g.drawString("Game Over", (ScreenWidth - metrics.stringWidth(("Game Over"))) / 2, ScreenHight / 2);
        }
    }

    public void saveComponentAsImage(Component component) {
        BufferedImage image = new BufferedImage(component.getWidth(), component.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics g = image.getGraphics();
        component.paint(g);
        g.dispose();

        // Dialógus ablak megjelenítése a felhasználó számára
        JFileChooser fileChooser = new JFileChooser();
        int result = fileChooser.showSaveDialog(null);

        if (result == JFileChooser.APPROVE_OPTION) {
            // Kiválasztott fájl elérési útjának lekérdezése
            File selectedFile = fileChooser.getSelectedFile();

            try {
                // Kép mentése a kiválasztott fájlba
                ImageIO.write(image, "png", selectedFile);
                JOptionPane.showMessageDialog(null, "Image saved successfully!");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public int getUNIT_SIZE() {
        return UNIT_SIZE;
    }
}
