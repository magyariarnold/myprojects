import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class SoundPlayer implements Runnable {

    private String filePath;
    private boolean condition;
    private Thread soundThread;

    public SoundPlayer(String filePath) {
        this.filePath = filePath;
        soundThread = new Thread(this);
    }

    public void startContinuousSoundPlayer() {
        soundThread = new Thread(this);
        soundThread.start();
        condition = true;
    }

    // lejatszik egy dalt es megall
    public void startSoundPlay() {
        try {
            //File URL relative to project folder
            AudioInputStream audioInputStream  = AudioSystem.getAudioInputStream(new File(filePath));
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            System.out.println("IO error");
        }
    }

    public void stop() {
        condition = false;
    }

    @Override
    public void run() {  // addig jatszodja amig meg nem allitom
        do {
            try {
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filePath));
                AudioFormat audioFormat = audioInputStream.getFormat();

                DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
                SourceDataLine sourceDataLine = (SourceDataLine) AudioSystem.getLine(info);
                sourceDataLine.open(audioFormat);
                sourceDataLine.start();

                int bufferSize = (int) audioFormat.getSampleRate() * audioFormat.getFrameSize();
                byte[] buffer = new byte[bufferSize];

                int bytesRead;
                while ((bytesRead = audioInputStream.read(buffer, 0, buffer.length)) != -1) {
                    sourceDataLine.write(buffer, 0, bytesRead);
                    if (!condition) sourceDataLine.stop();
                }

                sourceDataLine.drain();
                sourceDataLine.close();
                audioInputStream.close();
            } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
                e.printStackTrace();
            }
        } while (condition);
    }
}
