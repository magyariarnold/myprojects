import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;


public class Controller implements ActionListener {

    private final Snake snake;
    private final View view;
    private int DELAY = 65;
    Random random;
    Timer timer;

    public Controller(Snake snake, View view) {
        this.snake = snake;
        this.view = view;
    }

    public void startGame() {
        random = new Random();
        timer = new Timer(DELAY, this);
        newApple();
        timer.start();
    }

    public void newApple() {
        do {  // addig generaljuk az almat amig nincs a kigyo testen
            snake.setAppleX((int) random.nextInt(view.ScreenWidth / view.getUNIT_SIZE()) * view.getUNIT_SIZE());
            snake.setAppleY((int) random.nextInt(view.ScreenHight / view.getUNIT_SIZE()) * view.getUNIT_SIZE());
        } while (checkAppleOnBody(snake.getAppleY(), snake.getAppleX()));
    }

    private boolean checkAppleOnBody(int appleX, int appleY) {  // ellenorizzuk az almat a kigyora generaltae
        for (int i = 0; i < snake.getBodyParts(); i++) {
            if (appleX == snake.getX(i) && appleY == snake.getY(i)) {
                return true;
            }
        }
        return false;
    }

    public void move() {
        if (!snake.isPause()) {
            for (int i = snake.getBodyParts(); i > 0; i--) {
                snake.setX(i, snake.getX(i - 1));
                snake.setY(i, snake.getY(i - 1));
            }
        }

        switch (snake.getDirection()) {
            case 'U':
                snake.setY(0, snake.getY(0) - view.getUNIT_SIZE());
                break;
            case 'D':
                snake.setY(0, snake.getY(0) + view.getUNIT_SIZE());
                break;
            case 'L':
                snake.setX(0, snake.getX(0) - view.getUNIT_SIZE());
                break;
            case 'R':
                snake.setX(0, snake.getX(0) + view.getUNIT_SIZE());
                break;
            case 'S':
                view.saveComponentAsImage(view);
                snake.setDirection('P');
        }
    }

    public void gameOver() {
        timer.stop();
        saveScores();
        snake.setGameOver(true);
    }

    public void checkCollisions() {
        if (snake.getX(0) < 0 || snake.getX(0) >= view.ScreenWidth ||
                snake.getY(0) < 0 || snake.getY(0) >= view.ScreenHight) {
            gameOver();
        } else if (checkCollisionOnBody()) {
            gameOver();
        }
    }

    private boolean checkCollisionOnBody() {
        for (int i = 1; i < snake.getBodyParts(); i++) {
            if (snake.getX(0) == snake.getX(i) && snake.getY(0) == snake.getY(i)) {
                return true;
            }
        }
        return false;
    }

    public void checkApple() {
        if (snake.getX(0) == snake.getAppleX() && snake.getY(0) == snake.getAppleY()) {
            SoundPlayer appleSound = new SoundPlayer("src/sounds/appleEat.wav");
            appleSound.startSoundPlay();
            snake.setApplesEaten(snake.getApplesEaten() + 1);
            snake.setBodyParts(snake.getBodyParts() + 1);
            newApple();
        }
    }

    public int getDELAY() {
        return DELAY;
    }

    public void setDELAY(int DELAY) {
        this.DELAY = DELAY;
    }

    private void saveScores() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("src/scores.txt", true));
            writer.write(LocalDateTime.now().format(DateTimeFormatter.ofPattern("                             yyyy-MM-dd hh:mm:ss")) + " - ");  // szebb kiiratasert tele van tabbal, itt viszont nem szep kodban
            writer.write("Pontszam: " + snake.getApplesEaten());
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        move();
        checkCollisions();
        checkApple();
        view.repaint();
    }
}
