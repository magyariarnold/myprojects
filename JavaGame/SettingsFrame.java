import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class SettingsFrame extends JFrame {

    private String[] settings = new String[3];
    boolean snakeColorButtonPressed = false;  // ez a ket gomblenyomas erzekelo azert kellett hogy ne kelljen csinaljak meg szin menu itmeket, igy nezem melyik gombot nyomta a felhasznalo es azt allitom
    boolean snakeHeadColorButtonPressed = false;

    public SettingsFrame(int x, int y, int screenWidth, int screenHeight, Snake snake, View view, Controller controller, JFrame frame) {
        setVisible(false); // egybol eltunteti hogy ne pillanjon egyet
        readSettings("src/settings.txt", settings);
        setSettings(settings, snake, controller);
        getContentPane().setBackground(Color.DARK_GRAY);
        setBounds(x, y, screenWidth, screenHeight);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setLayout(new BorderLayout());
        setTitle("Settings");

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(4, 1));
        add(buttonPanel, BorderLayout.CENTER);

        JPanel labelsPanel = new JPanel(new GridLayout(4, 1));
        JLabel speedSetLabel = new JLabel("Speed");
        JLabel bodySetLabel = new JLabel("Body");
        JLabel headSetLabel = new JLabel("Head");
        speedSetLabel.setFont(new Font("Arial", Font.BOLD, 60));
        headSetLabel.setFont(new Font("Arial", Font.BOLD, 60));
        bodySetLabel.setFont(new Font("Arial", Font.BOLD, 60));
        labelsPanel.add(speedSetLabel);
        labelsPanel.add(headSetLabel);
        labelsPanel.add(bodySetLabel);
        add(labelsPanel, BorderLayout.WEST);

        JButton difficultyMenuButton = new JButton(settings[0]);
        difficultyMenuButton.setFont(new Font("Arial", Font.PLAIN, 60));
        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem easyMenuItem = new JMenuItem("Easy");
        JMenuItem mediumMenuItem = new JMenuItem("Medium");
        JMenuItem hardMenuItem = new JMenuItem("Hard");
        JMenuItem veryhardMenuItem = new JMenuItem("Very Hard");
        easyMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        mediumMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        hardMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        veryhardMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        popupMenu.add(easyMenuItem);
        popupMenu.add(mediumMenuItem);
        popupMenu.add(hardMenuItem);
        popupMenu.add(veryhardMenuItem);
        buttonPanel.add(difficultyMenuButton);

        difficultyMenuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                popupMenu.show(difficultyMenuButton, difficultyMenuButton.getWidth(), 0);
            }
        });

        easyMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                difficultyMenuButton.setText(easyMenuItem.getText());
                controller.setDELAY(100);
                settings[0] = "Easy";
                writeSettings("src/settings.txt", settings);
            }
        });

        mediumMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                difficultyMenuButton.setText(mediumMenuItem.getText());
                controller.setDELAY(65);
                settings[0] = "Medium";
                writeSettings("src/settings.txt", settings);
            }
        });

        hardMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                difficultyMenuButton.setText(hardMenuItem.getText());
                controller.setDELAY(40);
                settings[0] = "Hard";
                writeSettings("src/settings.txt", settings);
            }
        });

        veryhardMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                difficultyMenuButton.setText(veryhardMenuItem.getText());
                controller.setDELAY(25);
                settings[0] = "Very Hard";
                writeSettings("src/settings.txt", settings);
            }
        });

        JButton snakeColorButton = new JButton(settings[1]);
        snakeColorButton.setFont(new Font("Arial", Font.PLAIN, 60));
        JButton snakeHeadColorButton = new JButton(settings[2]);
        snakeHeadColorButton.setFont(new Font("Arial", Font.PLAIN, 60));
        JPopupMenu popupMenu1 = new JPopupMenu();
        JMenuItem blueMenuItem = new JMenuItem("Blue");
        JMenuItem redMenuItem = new JMenuItem("Red");
        JMenuItem orangeMenuItem = new JMenuItem("Orange");
        JMenuItem yellowMenuItem = new JMenuItem("Yellow");
        JMenuItem pinkMenuItem = new JMenuItem("Pink");
        JMenuItem greenMenuItem = new JMenuItem("Green");
        JMenuItem magentaMenuItem = new JMenuItem("Magenta");
        blueMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        redMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        orangeMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        yellowMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        pinkMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        greenMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));
        magentaMenuItem.setFont(new Font("Arial", Font.PLAIN, 60));

        popupMenu1.add(blueMenuItem);
        popupMenu1.add(redMenuItem);
        popupMenu1.add(orangeMenuItem);
        popupMenu1.add(yellowMenuItem);
        popupMenu1.add(pinkMenuItem);
        popupMenu1.add(greenMenuItem);
        popupMenu1.add(magentaMenuItem);
        buttonPanel.add(snakeColorButton);
        buttonPanel.add(snakeHeadColorButton);

        snakeColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                snakeHeadColorButtonPressed = false;
                snakeColorButtonPressed = true;
                popupMenu1.show(snakeColorButton, snakeColorButton.getWidth(), 0);
            }
        });

        snakeHeadColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                snakeColorButtonPressed = false;
                snakeHeadColorButtonPressed = true;
                popupMenu1.show(snakeColorButton, snakeColorButton.getWidth(), 0);
            }
        });

        blueMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (snakeColorButtonPressed) {
                    snakeColorButton.setText(blueMenuItem.getText());
                    snake.setSnakeColor(Color.blue);
                    settings[1] = "Blue";
                } else if (snakeHeadColorButtonPressed) {
                    snakeHeadColorButton.setText((blueMenuItem.getText()));
                    snake.setHeadColor(Color.blue);
                    settings[2] = "Blue";
                }
                writeSettings("src/settings.txt", settings);
            }
        });

        redMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (snakeColorButtonPressed) {
                    snakeColorButton.setText(redMenuItem.getText());
                    snake.setSnakeColor(Color.red);
                    settings[1] = "Red";
                } else if (snakeHeadColorButtonPressed) {
                    snakeHeadColorButton.setText(redMenuItem.getText());
                    snake.setHeadColor(Color.red);
                    settings[2] = "Red";
                }
                writeSettings("src/settings.txt", settings);
            }
        });

        orangeMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (snakeColorButtonPressed) {
                    snakeColorButton.setText(orangeMenuItem.getText());
                    snake.setSnakeColor(Color.orange);
                    settings[1] = "Orange";
                } else if (snakeHeadColorButtonPressed) {
                    snakeHeadColorButton.setText(orangeMenuItem.getText());
                    snake.setHeadColor(Color.orange);
                    settings[2] = "Orange";
                }
                writeSettings("src/settings.txt", settings);
            }
        });

        yellowMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (snakeColorButtonPressed) {
                    snakeColorButton.setText(yellowMenuItem.getText());
                    snake.setSnakeColor(Color.yellow);
                    settings[1] = "Yellow";
                } else if (snakeHeadColorButtonPressed) {
                    snakeHeadColorButton.setText(yellowMenuItem.getText());
                    snake.setHeadColor(Color.yellow);
                    settings[2] = "Yellow";
                }
                writeSettings("src/settings.txt", settings);
            }
        });

        pinkMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (snakeColorButtonPressed) {
                    snakeColorButton.setText(pinkMenuItem.getText());
                    snake.setSnakeColor(Color.pink);
                    settings[1] = "Pink";
                } else if (snakeHeadColorButtonPressed) {
                    snakeHeadColorButton.setText(pinkMenuItem.getText());
                    snake.setHeadColor(Color.pink);
                    settings[2] = "Pink";
                }
                writeSettings("src/settings.txt", settings);
            }
        });

        greenMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (snakeColorButtonPressed) {
                    snakeColorButton.setText(greenMenuItem.getText());
                    snake.setSnakeColor(Color.green);
                    settings[1] = "Green";
                } else if (snakeHeadColorButtonPressed) {
                    snakeHeadColorButton.setText(greenMenuItem.getText());
                    snake.setHeadColor(Color.green);
                    settings[2] = "Green";
                }
                writeSettings("src/settings.txt", settings);
            }
        });

        magentaMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (snakeColorButtonPressed) {
                    snakeColorButton.setText(magentaMenuItem.getText());
                    snake.setSnakeColor(Color.magenta);
                    settings[1] = "Magenta";
                } else if (snakeHeadColorButtonPressed) {
                    snakeHeadColorButton.setText(magentaMenuItem.getText());
                    snake.setHeadColor(Color.magenta);
                    settings[2] = "Magenta";
                }
                writeSettings("src/settings.txt", settings);
            }
        });

        JButton backButton = new JButton("Back");
        backButton.setFont(new Font("Arial", Font.PLAIN, 60));
        buttonPanel.add(backButton);

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                frame.setVisible(true);
            }
        });
    }

    private void setSettings(String[] settings, Snake snake, Controller controller) {
        JMenuItem blueMenuItem = new JMenuItem("Blue");
        JMenuItem redMenuItem = new JMenuItem("Red");
        JMenuItem orangeMenuItem = new JMenuItem("Orange");
        JMenuItem yellowMenuItem = new JMenuItem("Yellow");
        JMenuItem pinkMenuItem = new JMenuItem("Pink");
        JMenuItem greenMenuItem = new JMenuItem("Green");
        JMenuItem magentaMenuItem = new JMenuItem("Magenta");
        snake.setSpeed(settings[0]);
        switch (settings[1]) {
            case "Blue":
                snake.setSnakeColor(Color.blue);
              break;
            case "Red":
                snake.setSnakeColor(Color.red);
                break;
            case "Orange":
                snake.setSnakeColor(Color.orange);
                break;
            case "Yellow":
                snake.setSnakeColor(Color.yellow);
                break;
            case "Pink":
                snake.setSnakeColor(Color.pink);
                break;
            case "Green":
                snake.setSnakeColor(Color.green);
                break;
            case "Magenta":
                snake.setSnakeColor(Color.magenta);
                break;
        }

        switch (settings[2]) {
            case "Blue":
                snake.setHeadColor(Color.blue);
                break;
            case "Red":
                snake.setHeadColor(Color.red);
                break;
            case "Orange":
                snake.setHeadColor(Color.orange);
                break;
            case "Yellow":
                snake.setHeadColor(Color.yellow);
                break;
            case "Pink":
                snake.setHeadColor(Color.pink);
                break;
            case "Green":
                snake.setHeadColor(Color.green);
                break;
            case "Magenta":
                snake.setHeadColor(Color.magenta);
                break;
        }

        switch (snake.getSpeed()) {
            case "Easy" -> controller.setDELAY(100);
            case "Medium" -> controller.setDELAY(65);
            case "Hard" -> controller.setDELAY(40);
            case "Very Hard" -> controller.setDELAY(25);
        }
    }

    private void readSettings(String path, String[] settings) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String line;
            int i = 0;
            while ((line = reader.readLine()) != null) {
                settings[i] = line;
                i++;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void writeSettings(String path, String[] settings) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path));
            for (String setting : settings) {
                writer.write(setting);
                writer.flush();
                writer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
