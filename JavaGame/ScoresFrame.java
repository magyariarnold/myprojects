import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ScoresFrame extends JFrame {

    public ScoresFrame(int x, int y, int screenWidth, int screenHeight, JFrame previousframe, Snake snake) {
        setVisible(false);
        getContentPane().setBackground(Color.white);
        setBounds(x, y, screenWidth, screenHeight);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setLayout(new BorderLayout());
        setTitle("Scores");

        ArrayList<Integer> scores = new ArrayList<>();
        ArrayList<String> dates = new ArrayList<>();
        ArrayList<String> times = new ArrayList<>();

        JPanel buttonPanel = new JPanel(new GridLayout(5, 1));
        add(buttonPanel, BorderLayout.SOUTH);
        JButton backButton = new JButton("Back");
        JButton bestScoreButton = new JButton("Show Best Score");
        JButton latestDateButton = new JButton("Latest Date");
        JButton latestTimeButton = new JButton("Latest Time");
        JButton totalPointsButton = new JButton("Total Points");
        buttonPanel.add(bestScoreButton);
        buttonPanel.add(latestDateButton);
        buttonPanel.add(latestTimeButton);
        buttonPanel.add(totalPointsButton);
        buttonPanel.add(backButton);

        // a legjobb eredmenyek kiszurese streamek hasznalataval
        JPanel topPart = new JPanel(new FlowLayout());
        JTextArea textAreaForResult = new JTextArea(1, 1);
        topPart.add(textAreaForResult);
        textAreaForResult.setEditable(false);
        textAreaForResult.setFont(new Font("Arial", Font.BOLD, 35));

        bestScoreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                add(topPart, BorderLayout.NORTH);
                textAreaForResult.setText(Objects.requireNonNull(scores.stream().max(Integer::compareTo).orElse(null)).toString());
            }
        });

        latestDateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                add(topPart, BorderLayout.NORTH);
                textAreaForResult.setText(dates.stream().map(s -> LocalDate.from(DateTimeFormatter.ofPattern("yyyy-MM-dd").parse(s))).max(LocalDate::compareTo).orElse(null).toString());
            }
        });

        latestTimeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                add(topPart, BorderLayout.NORTH);
                textAreaForResult.setText(times.stream().map(s -> LocalTime.from(DateTimeFormatter.ofPattern("HH:mm:ss").parse(s))).max(LocalTime::compareTo).orElse(null).toString());
            }
        });

        totalPointsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                add(topPart, BorderLayout.NORTH);
                textAreaForResult.setText(scores.stream().reduce(0, Integer::sum).toString());
            }
        });

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                previousframe.setVisible(true);
            }
        });

        JTextArea textArea = new JTextArea(20, 1);
        textArea.setEditable(false);
        textArea.setFont(new Font("Arial", Font.BOLD, 35));
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        add(scrollPane, BorderLayout.CENTER);

        try {
            BufferedReader reader = new BufferedReader(new FileReader("src/scores.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                textArea.append(line + "\n");
                String[] parts = line.split(" ");
                dates.add(parts[29]);
                times.add(parts[30]);
                scores.add(Integer.parseInt(parts[33]));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
