public class Main {
    public static void main(String[] args) {
        Snake snake = new Snake(10, 500);
        View view = new View(snake, 1200, 800);
        Controller controller = new Controller(snake, view);
        new MenuFrame(1100, 200, 1200, 800, snake, view, controller);
    }
}