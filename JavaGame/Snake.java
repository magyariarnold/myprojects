import java.awt.*;

public class Snake {

    private int bodyParts;
    private int[] x;
    private int[] y;
    private int appleX, appleY, applesEaten = 0;
    private char direction = 'R';
    private boolean gameOver = false;
    private boolean pause = false;
    private Color headColor = Color.green;
    private Color snakeColor = Color.blue;
    private String speed = "Easy";

    public Snake(int bodyParts, int MaxSize) {
        this.bodyParts = bodyParts;
        this.x = new int[MaxSize];
        this.y = new int[MaxSize];
    }

    public int getBodyParts() {
        return bodyParts;
    }

    public void setBodyParts(int bodyParts) {
        this.bodyParts = bodyParts;
    }

    public char getDirection() {
        return direction;
    }

    public void setDirection(char direction) {
        this.direction = direction;
    }

    public int getX(int index) {
        return x[index];
    }

    public void setX(int index, int value) {
        x[index] = value;
    }

    public int getY(int index) {
        return y[index];
    }

    public void setY(int index, int value) {
        y[index] = value;
    }

    public int getAppleX() {
        return appleX;
    }

    public void setAppleX(int appleX) {
        this.appleX = appleX;
    }

    public int getAppleY() {
        return appleY;
    }

    public void setAppleY(int appleY) {
        this.appleY = appleY;
    }

    public int getApplesEaten() {
        return applesEaten;
    }

    public void setApplesEaten(int applesEaten) {
        this.applesEaten = applesEaten;
    }

    public boolean isPause() {
        return pause;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public Color getHeadColor() {
        return headColor;
    }

    public void setHeadColor(Color headColor) {
        this.headColor = headColor;
    }

    public Color getSnakeColor() {
        return snakeColor;
    }

    public void setSnakeColor(Color snakeColor) {
        this.snakeColor = snakeColor;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
