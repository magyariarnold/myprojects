import javax.swing.*;

public class GameFrame extends JFrame {

    static final int SCREEN_WIDTH = 1200;
    static final int SCREEN_HEIGHT = 800;

    public GameFrame(View view, Controller controller) {
        setBounds(1100, 200, SCREEN_WIDTH + 15, SCREEN_HEIGHT + 38);  // a szelesseghez es magassaghoz hozzakellett adni hogy legyen kicsit nagyobb az ablak hogy jojjenek ki a kockak
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Snake");

        // Jatek panel
        add(view);
        controller.startGame();

        setVisible(true);
    }
}
